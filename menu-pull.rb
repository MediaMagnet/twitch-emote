#!/usr/bin/env ruby
require 'oj'
require 'rest-client'
require 'open-uri'
require 'fileutils'
require 'configatron'
require 'progressbar'
require 'tty-prompt'
require_relative 'config.rb'

$dirname = 'emotegrab'
$name = nil
$login = nil
$answer = nil
$prompt = TTY::Prompt.new

unless File.directory?($dirname)
	FileUtils.mkdir_p($dirname)
end

def get_emote
	Oj.load(open("http://twitchemotes.com/api_cache/v3/global.json"))
end

def globalget

	globalprogressbar = ProgressBar.create(:starting_at => 0, :total => 188 )
	for code in get_emote.keys
		#print ("Pulling " + code + "\n" )
		globalprogressbar.increment
		id = get_emote[code]['id']
		download = open("https://static-cdn.jtvnw.net/emoticons/v1/#{id}/1.0")
		IO.copy_stream(download, "./#{$dirname}/#{code}.png")
	end
end


def find_channel
	puts "Let's grab a set of subscriber emotes. Which channel would you like? \n"
	$name = gets
	name1 = $name.chomp
	login = RestClient.get "https://api.twitch.tv/helix/users?login=#{name1}", {:'Client-ID' => configatron.clientid}
	$login = Oj.load(login).fetch('data').first.each_with_index { |val| }.values_at('id').to_s.delete('[{}]\"')
	puts "Found the channel you requested, give me a quick second to load their data. \n"
end

def subget
	find_channel
	emsource = RestClient.get "https://twitchemotes.com/api_cache/v3/subscriber.json"
	em_loaded = Oj.load(emsource).fetch($login).each_with_index{|val|}
	subparse = em_loaded.fetch('emotes').each_with_index{|val|}
	tcount = subparse.count
	subprogressbar = ProgressBar.create(:starting_at => 0, :total => tcount)
	i = 0

	loop do
		id = subparse[i].fetch('id')
		code = subparse[i].fetch('code')
		#print("Grabing #{code} from channel \n")
		subprogressbar.increment
		download = open("https://static-cdn.jtvnw.net/emoticons/v1/#{id}/1.0")
		IO.copy_stream(download, "./#{$dirname}/#{code}.png")
		i += 1
		if i == tcount
			break
		end
	end
end

puts "What emotes did you want to pull? (Enter Global or Subscriber.)"
$answer = gets
$answer = $answer.downcase

if $answer == 'global'
	globalget
elsif $answer == 'subscriber'
	subget
end
