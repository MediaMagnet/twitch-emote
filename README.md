# Emote Puller #

A short script to pull emotes using twitchemotes api version 3

## Requirements ##

Requires Ruby Version 2.4.0 or newer

- Oj
- Rest-Client
- Open-URI
- Fileutils
- Configatron
- ProgressBar

## To use: ##

1. Clone this repo
2. run ```bundle install```
3. copy example.config.rb to config.rb
4. Set your twitch clientid in config.rb
5. follow prompts in script 


Note: things changing for ruby instructions

emotes can be saved as:

- Large (112x112)
- Medium (56x56)
- Small (28x28)

If you want to pull the Turbo/Prime emotes use --twitch-turbo-- when you get to the channel name prompt.
