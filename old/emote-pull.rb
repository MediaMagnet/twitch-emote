#!/usr/bin/env ruby

require 'json'
require 'open-uri'
require 'fileutils'

=begin
Emote pull attempt using ruby
lets first make sure the directory for this is there
=end

dirname = 'emotesruby'
unless File.directory?(dirname)
    FileUtils.mkdir_p(dirname)
end

def get_emote

source = open("https://twitchemotes.com/api_cache/v3/global.json")
emotes = JSON.dump(JSON.load(source))

end

for code in get_emote.each do

	print ('emote ' + code)
end

