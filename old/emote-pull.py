#!/usr/bin/env python3

#Code written by MediaMagnet, Sersium, and Arkun
#Requires python3 to be installed
#uses twitchemotes api version 2 to pull emotes
#if issues arrise contact me via gitlab

#import the basics
import urllib.request
import os
import json
import enchant

#getting cursesmenu working:
from cursesmenu import *
from cursesmenu.items import *

#for global emotes
def globalemote():
	print('Grabbing emote list...')
#	emotes = json.loads(urllib.request.urlopen('https://twitchemotes.com/api_cache/v2/global.json').read().decode("utf-8"))
	emotes = json.loads(urllib.request.urlopen('https://twitchemotes.com/api_cache/v3/global.json').read().decode("utf-8"))
	for code, id in emotes.keys['emotes'].items():
		print('Downloading: ' + code + '...')
		urllib.request.urlretrieve(emotes['id'].replace('{id}', str(emote['id'])),
			                   './emotes/' + code + '.png')
	print('Done pulling global emotes')

#for channel subscriber emotes
def chanpull():

	subdict = enchant.request_pwl_dict("./sublist.txt")
	twiname1 = str(input("What channel would you like me to pull? (use --twitch-turbo-- for Twitch prime/turbo emotes) ").lower())
	if subdict.check(twiname1):
#		emotes = json.loads(urllib.request.urlopen('https://twitchemotes.com/api_cache/v2/subscriber.json').read().decode("utf-8"))
		emotes = json.loads(urllib.request.urlopen('https://twitchemotes.com/api_cache/v3/subscriber.json').read().decode("utf-8"))
		for emote in emotes['channels'][twiname1]['emotes']:
			code = emote['code']
			print('Downloading: ' + code + '...')
			print(emotes['template'][sizevar].replace('{image_id}', str(emote['image_id'])),
						   './emotes/' + code + '.png')
			urllib.request.urlretrieve(emotes['template'][sizevar].replace('{image_id}', str(emote['image_id'])),
						   './emotes/' + code + '.png')
		print('Done pulling ' + twiname1 + ' emotes')
	else:
		oops = input("Username: " + twiname1 + " not found. Would you like to attempt anyway?")
		if oops=="yes" or oops=="y":
#			emotes = json.loads(urllib.request.urlopen('https://twitchemotes.com/api_cache/v2/subscriber.json').read().decode("utf-8"))
			emotes = json.loads(urllib.request.urlopen('https://twitchemotes.com/api_cache/v3/subscriber.json').read().decode("utf-8"))
			for emote in emotes['channels'][twiname1]['emotes']:
				code = emote['code']
				print('Downloading: ' + code + '...')
				print(emotes['template'][sizevar].replace('{image_id}', str(emote['image_id'])),
						   './emotes/' + code + '.png')
			urllib.request.urlretrieve(emotes['template'][sizevar].replace('{image_id}', str(emote['image_id'])),
						   './emotes/' + code + '.png')
			print('Done pulling ' + twiname1 + ' emotes')
		elif oops=="no" or oops=="n":
			chanpull()
		else:
			print("Enter either yes/no")

#for everything


#setting folder path to save emotes	
if not os.path.exists('./emotes'):
	os.makedirs('./emotes')
	
print('Twitch Emote puller v 0 uses twitchapi version 2')
print('Saving emotes to folder: ' + os.path.abspath('./emotes') + '...')
#asking what size emotes you want small 28x28 medium 56x56 large 112x112
sizevar = str(input("Would you like the small (28x28px), medium (56x56px), or large (112x112px) emotes? "))

#emote menu items
emenu = CursesMenu("Emote Puller","Would you like global or channel emotes?")
globalem = FunctionItem("Global Emotes", globalemote)
channlem = FunctionItem("Subscriber Emotes", chanpull)
#Great let's build the menu now.
emenu.append_item(globalem)
emenu.append_item(channlem)
emenu.show()
