#!/usr/bin/env ruby

require 'json'
require 'open-uri'
require 'fileutils'
require 'oj'

#Create the directorys needed

dirname = 'emotesruby'
unless File.directory?(dirname)
    FileUtils.mkdir_p(dirname)
end

#tell the user where the files will be saved

print("Emotes will be saved in ./" + dirname + "\n")

#Creating a local copy of the Subscriber JSON to make it easier to put in a filestream

%x( wget https://twitchemotes.com/api_cache/v3/subscriber.json )
puts "Converting from Single line to pretty print json"
%x( python3 -m json.tool subscriber.json > subscriber1.json)
%x( mv subscriber1.json subscriber.json)
system "clear"

#set the url of the global list.
def get_emote

Oj.load(open("https://twitchemotes.com/api_cache/v3/global.json"))

end

#pull the global list
for code in get_emote.keys

    print ("Pulling " + code + "\n" )
    id = get_emote[code]['id']
    download = open("https://static-cdn.jtvnw.net/emoticons/v1/#{id}/1.0")
    IO.copy_stream(download, "./#{dirname}/#{code}.png")
end


#set the url of the subscriber list
=begin
def get_sub

Oj.load(open("./subscriber.json"))

end
=end

=begin
pull the subscriber list, the end goal is to have it ask which channel is wanted as this is a really big list, 
however I'm not sure on how to convert the channel_name to the id possibly somekinda look up?

from what i saw the [0] is needed for it to grab the first item however it only pulls the first item
if its changed to [22] it'll pull that one so what I can't figure out is how to increment the list.
=end

#we have the json lets put it in a file stream.

file = File.read('subscriber.json')
get_sub = JSON.parse(file)

for channel_id in get_sub.keys
   
    name1 = get_sub[channel_id]['display_name']
    print ("Pulling channel emotes for " + name1 + "\n")
    emotes1 = get_sub[channel_id]['emotes']
    get_sub[channel_id]['emotes'].select do |emote|
        id = emotes1[0..-1][id]
        emcode = emotes1[code]
        print("grabbing " + emcode + "\n")
        download = open("https://static-cdn.jtvnw.net/emoticons/v1/#{id}/1.0")
        IO.copy_stream(download, "./#{dirname}/#{emcode}.png")
   end 
end
