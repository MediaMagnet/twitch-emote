#!/usr/bin/env ruby
#!set -x
=begin
Emote-Pull v2 
Based on Python script wrote by MediaMagnet and Stump at AGDQ 2017
=end

require 'oj'
require 'rest-client'
require 'open-uri'
require 'fileutils'
require 'configatron'
require 'progressbar'
require 'tty-prompt'
require_relative 'config.rb'

#some vars to make things a bit easier to handle later.
$name = nil
$login = nil
$globalemcount = nil
$prompt = TTY::Prompt.new
$subagain = nil

#clean up the screen and start doing stuff

system('clear')

puts "Welcome to emote-pull v2! \nEmotes will be saved in ./#{configatron.directory} \n"

#Let's get some info like what size emotes are needed.

choices = {'Small' => 1.0, 'Medium' => 2.0, 'Large' => 3.0}

$size = $prompt.enum_select("What size emote would you like?", choices)

unless File.directory?(configatron.directory)
	FileUtils.mkdir_p(configatron.directory)
end


def get_emote
	Oj.load(open("http://twitchemotes.com/api_cache/v3/global.json"))
end

puts "Pulling Global emotes, give me a second."
$globalemcount = get_emote.count

def globalem
	globalprogressbar = ProgressBar.create(:starting_at => 0, :total => $globalemcount )

	for code in get_emote.keys
		#print ("Pulling " + code + "\n" )
		globalprogressbar.increment
		id = get_emote[code]['id']
		download = open("https://static-cdn.jtvnw.net/emoticons/v1/#{id}/#{$size}")
		IO.copy_stream(download, "./#{configatron.directory}/#{code}.png")
	end
end

def find_channel
	#puts "Let's grab a set of subscriber emotes. Which channel would you like? \n"
	#$name = gets
	$name = $prompt.ask("Which channel would you like?")
	name1 = $name.chomp
	login = RestClient.get "https://api.twitch.tv/helix/users?login=#{name1}", {:'Client-ID' => configatron.clientid}
	$login = Oj.load(login).fetch('data').first.each_with_index { |val| }.values_at('id').to_s.delete('[{}]\"')
	puts "Found the channel you requested, give me a quick second to load their data. \n"

	sleep 1
end

def subem
	puts $login.class
	puts $login
	emsource = RestClient.get "https://twitchemotes.com/api_cache/v3/subscriber.json"
	em_loaded = Oj.load(emsource).fetch($login).each_with_index{|val|}
	subparse = em_loaded.fetch('emotes').each_with_index{|val|}
	tcount = subparse.count
	subprogressbar = ProgressBar.create(:starting_at => 0, :total => tcount)
	i = 0
	firstem = subparse[i].fetch('code')

	#some kinda confirmation of channel. have script print first emote in a question.
	em_confirm = $prompt.yes?("Is #{firstem} from the channel you are trying to retreve?")
	
#loop through each emote so we get them all instead of just the first one.
	if em_confirm == true
		puts "Got it! pulling emotes now."
		loop do
			id = subparse[i].fetch('id')
			code = subparse[i].fetch('code')
			#print("Grabing #{code} from channel \n")
			subprogressbar.increment
			download = open("https://static-cdn.jtvnw.net/emoticons/v1/#{id}/#{$size}")
			IO.copy_stream(download, "./#{configatron.directory}/#{code}.png")
			i += 1
			if i == tcount
				break
			end
		end
	elsif em_confirm == false
		puts "Okay. No problem!"		
	end
end

def another_run
	$subagain = $prompt.yes?("Would you like to pull another channel's emotes?") do |q|
		q.default false
	end
end

#Make sure this can handle more than two channels before breaking
def askme
	if $subagain == true
		puts "Okay, great! Let's grab another channel."
		find_channel
		subem
		system('clear')
		another_run
		askme
	elsif $subagain == false
		puts "Okay, thank you for using emote-pull. Run this script again if needed"
	end
end
#let's talk to the twitch api and get the stuff we need.

#global needed?

globalask = $prompt.yes?('Do you need global emotes?') do |q|
	q.default true
end

if globalask == true
	globalem
end

system('clear')
puts "We're done with the global emotes. Let's take a look at subscriber emotes."
find_channel

#ok we got what we need let's do stuff.
subem
#run again?
another_run
askme
